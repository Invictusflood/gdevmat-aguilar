void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, -(height/2) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0); 
}

float t = 0;

void draw()
{
  float perlin = noise(t);
  float mappedValue = map(noise(t), 0, 1, 0, Window.top);
  println(mappedValue);
  
  rect(Window.left + (t*100), Window.bottom, 1, mappedValue);
  
  t+= 0.01f;
}

/*
Randomness:

random()
equal weighted chance

randomGaussian()
Normal distribution using standard deviation
(Refer to Quiz 1)

New Funtions:
map();
rect();
*/
