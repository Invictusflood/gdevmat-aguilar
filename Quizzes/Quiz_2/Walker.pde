class Walker
{
  float xPosition, yPosition, size, r, g, b;
  float[] t = {0,1,2,3,4,5};
  
  Walker()
  {
  }
  
  void render()
  {
    fill(r, g, b, 255);
    circle(xPosition, yPosition, size);
  }
  
  void randomWalk()
  {
    xPosition = map(noise(t[0]), 0, 1, Window.left, Window.right);
    yPosition = map(noise(t[1]), 0, 1, Window.bottom, Window.top);
    size = map(noise(t[2]), 0, 1, 10, 80);
    r = map(noise(t[3]), 0, 1, 0, 255);
    g = map(noise(t[4]), 0, 1, 0, 255);
    b = map(noise(t[5]), 0, 1, 0, 255);
    
    for(int i = 0; i < t.length; i++)
    {
      t[i]+= 0.01f;
    }
  }
}
