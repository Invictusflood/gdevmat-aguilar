int frameMax;

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, -(height/2) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, -1, 0); 
  background(0);
  frameMax = frameCount + 1000;
}

void draw()
{
  float gaussian = randomGaussian();
  float mean = random(-(width/2),(width/2));
  float standardDeviation = 240;

  float x = (standardDeviation * gaussian) + mean;
  float y = random(-(height/2),(height/2));
  
  mean = random(-25, 25);
  standardDeviation = 50;
  
  float size = (standardDeviation * gaussian) + mean;

  noStroke();
  fill(random(255), random(255), random(255));
  circle(x, y, size);
  
  if(frameCount == frameMax)
  {
    setup();
  }
}
