Mover movers[];

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, -(height/2) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0);
  background(0);
  // Set the starting position of the shape
  movers = new Mover[10];
  float mass = 1;
  float scale = 50;
  for(int i = 0; i < movers.length; i++)
  {
    movers[i] = new Mover(Window.left, 0, scale * mass);
    movers[i].r = map(noise(random(255)), 0, 1, 0, 255);
    movers[i].g = map(noise(random(255)), 0, 1, 0, 255);
    movers[i].b = map(noise(random(255)), 0, 1, 0, 255);
    movers[i].setMass(mass);
    mass += 0.2;
  }
  //mover.acceleration = new PVector(0.1, 0);
}

PVector wind = new PVector(0.1f, 0);
PVector gravity = new PVector(0, -1);

void draw() 
{
  background(0);
  noStroke();
  for(int i = 0; i < movers.length; i++)
  {
    movers[i].render();
  
    movers[i].applyForce(wind);
    movers[i].applyForce(gravity);
  
    if(movers[i].position.y < Window.bottom)
    {
      movers[i].velocity.y *= -1;
      movers[i].position.y = Window.bottom; //Safeguard
    }
    
    if(movers[i].position.x > Window.right)
    {
      movers[i].velocity.x *= -1;
      movers[i].position.x = Window.right; //Safeguard
    }
  }
}
