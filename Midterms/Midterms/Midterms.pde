Walker blackhole, circles[];
int frameMax, arraySize = 100;
float[] t = {0,1,2};
void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  circles = new Walker[arraySize];
  setUpWalkers();
  frameMax = frameCount + 150;
}

void setUpWalkers()
{
  blackhole = new Walker(random(Window.left, Window.right), random(Window.bottom, Window.top), 50);
  for (int i = 0; i < circles.length; i++)
  {
    //Reference: https://processing.org/examples/randomgaussian.html
    float gaussian = randomGaussian();
    float sd = 60;
    float meanX = random(Window.left, Window.right);
    float meanY = random(Window.bottom, Window.top);
    float x = (sd * gaussian) + meanX;
    float y = (sd * gaussian) + meanY;
    float r = map(noise(t[0]), 0, 1, 0, 255);
    float g = map(noise(t[1]), 0, 1, 0, 255);
    float b = map(noise(t[2]), 0, 1, 0, 255);
    circles[i] = new Walker(x, y, random(5, 40)); 
    circles[i].setColor(r, g, b, 255);
    for(int p = 0; p < t.length; p++)
    {
      t[p] += 1.00f;
    }
  }
}

void renderCircles()
{
  for (int i = 0; i < circles.length; i++)
  {
    circles[i].render();
    PVector direction = PVector.sub(blackhole.position, circles[i].position);
    direction.normalize(); //Normalize to 1
    direction.mult(10); //To change speed
    circles[i].position.add(direction);
  }
}

void draw()
{
  if(frameCount >= frameMax)
  {
    setup();
  }
  background(0);
  noStroke();
  renderCircles();
  blackhole.render();
}
