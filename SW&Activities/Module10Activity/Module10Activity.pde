Mover[] movers;
Liquid ocean = new Liquid(0, -100, Window.right, Window.bottom, 0.1f);

int frameMax;

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  frameMax = frameCount + 300;
  movers = new Mover[10];
  int xAdd = 100;
  int moverMass = 11;
  for (int i = 0; i < movers.length; i++)
  {
    movers[i] = new Mover(Window.left + xAdd, 200);
    movers[i].mass = moverMass;
    movers[i].setColor(random(1, 255),random(1, 255),random(1, 255), random(150, 255));
    xAdd += 100;
    moverMass -= 1;
  }
}

PVector wind = new PVector(0.2f, 0);


void draw()
{
  if(frameCount >= frameMax)
  {
    setup();
  }
  
  background(255);
  ocean.render();
  noStroke();

  for (Mover m : movers)
  {
    m.applyGravity();
    m.applyFriction();

    
    m.render();
    m.update();
    
    if (m.position.x > Window.right)
    {
      m.velocity.x *= -1;
      m.position.x = Window.right;
    }
    
    if (m.position.y < Window.bottom)
    {
      m.velocity.y *= -1; 
      m.position.y = Window.bottom;
    }
    
    if (ocean.isCollidingWith(m))
    {
      m.applyForce(ocean.calculateDragForce(m)); 
    }
    else
    {
      m.applyForce(wind);
    }
  }
}
