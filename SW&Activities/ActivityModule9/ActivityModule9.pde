Mover mover;
Mover[] movers;

int frameMax;

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  frameMax = frameCount + 600;
  
  //mover = new Mover();
  movers = new Mover[5];
  //mover.position.x = Window.left + 50;
  //mover.mass = 10;
  //mover.acceleration = new PVector(0.1, 0);
  int yDistance = 50;
  for (int i = 0; i < movers.length; i++)
  {
    movers[i] = new Mover(Window.left + 50, Window.top - yDistance);
    movers[i].mass = random(1, 10);
    movers[i].setColor(random(1, 255),random(1, 255),random(1, 255), random(150, 255));
    yDistance += 144;
  }
}

PVector wind = new PVector(0.2f, 0);


void draw()
{
  if(frameCount >= frameMax)
  {
    setup();
  }
  
  background(255);
  noStroke();
  for (Mover m : movers)
  {
    //m.applyGravity();
    if(m.velocity.x >= 0)
      m.applyFriction(0.1);
    else
      m.velocity.x = 0;
    
    if(m.position.x < 0)
    {
      m.applyForce(wind);
    }
    m.render();
    m.update();
    
    if (m.position.x > Window.right)
    {
      m.velocity.x *= -1;
      m.position.x = Window.right;
    }
    
    if (m.position.y < Window.bottom)
    {
      m.velocity.y *= -1; 
      m.position.y = Window.bottom;
    }
  }
  

}
