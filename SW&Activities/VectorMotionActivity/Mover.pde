public class Mover
{
   PVector position = new PVector();
   PVector velocity = new PVector();
   PVector acceleration = new PVector();
   public float scale = 50;
   public float r = 255, g = 255, b = 255, a = 255;
   Mover()
   {
      position = new PVector(); 
   }
   
   Mover(float x, float y)
   {
      position = new PVector(x, y);
   }
   
   
   Mover(float x, float y, float scale)
   {
      position = new PVector(x, y);
      this.scale = scale;
   }
   
   Mover(PVector position)
   {
      this.position = position; 
   }
   
   Mover(PVector position, float scale)
   {
      this.position = position; 
      this.scale = scale;
   }
   
   
   public void render()
   {
      fill(r,g,b,a);
      circle(position.x, position.y, scale);
      update();
   }
   
   private void update()
   {
      velocity.add(this.acceleration);
      position.add(velocity);
   }
   
   
   public void setColor(float r, float g, float b, float a)
   {
      this.r = r;
      this.g = g;
      this.b = b;
      this.a = a;
   }
}
