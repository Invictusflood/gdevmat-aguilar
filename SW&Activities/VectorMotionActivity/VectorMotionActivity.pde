Mover mover;

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, -(height/2) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0);
  background(0);
  // Set the starting position of the shape
  mover = new Mover();
  mover.position.x = Window.left + 50;
  mover.acceleration = new PVector(0.1, 0);
}

void draw() 
{
  background(102);
  mover.render();
  
  if (mover.position.x > 0 && mover.velocity.x > 0) {
    mover.acceleration.x = -0.1;
  }
  
  if (mover.velocity.x <= 0)
  {
    mover.acceleration.x = 0;
    mover.velocity.limit(0);
  }
}
