class Walker
{
  float xPosition;
  float yPosition;
  
  Walker()
  {
  }
  
  Walker(float x, float y)
  {
    xPosition = x;
    yPosition = y;
  }
  
  void render()
  {
    circle(xPosition, yPosition, 30);
  }
  
  void randomWalk(int distance)
  {
    int direction = int(random(8));
    switch(direction)
    {
      case 0:
        yPosition += distance;
        break;
      case 1:
        yPosition -= distance;
        break;
      case 2:
        xPosition += distance;
        break;
      case 3:
        xPosition -= distance;
        break;
      case 4:
        yPosition += distance;
        xPosition += distance;
        break;
      case 5:
        yPosition += distance;
        xPosition -= distance;
        break;
      case 6:
        yPosition -= distance;
        xPosition += distance;
        break;
      case 7:
        yPosition -= distance;
        xPosition -= distance;
        break;
    }
  }
}
