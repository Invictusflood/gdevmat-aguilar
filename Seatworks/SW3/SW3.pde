float[] t = {0,1,2,3,4};

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, -(height/2) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, -1, 0);
}

PVector randPos()
{
  float x = map(noise(t[0]), 0, 1, 0, width) - Window.windowWidth;
  float y = -(map(noise(t[1]), 0, 1, 0, height) - Window.windowHeight);

  return new PVector(x, y);
}

void draw()
{
  background(0);
  //Sabre
  renderLine(300, 7, map(noise(t[2]), 0, 1, 0, 255), 
  map(noise(t[3]), 0, 1, 0, 255), map(noise(t[4]), 0, 1, 0, 255));
  //Hilt
  renderLine(50, 10, 128, 128, 128);
  
  for(int i = 0; i < t.length; i++)
    {
      t[i]+= 0.01f;
    }
}

void renderLine(float lineLength, float thickness, float r, float g, float b)
{
  PVector line = randPos();
  line.normalize();
  line.mult(lineLength);
  strokeWeight(thickness);
  stroke(r, g, b);
  line(-line.x, -line.y, line.x, line.y);
}
